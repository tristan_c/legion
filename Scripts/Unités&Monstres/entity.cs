﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {
	
    public bool isMonster = false;
    public float moveSpeed = 1;
    public float attackRange = 0.30f;
    public float attackCooldown = 3;

	private GameObject bullet;

	private Vector3 moveDirection;
	public Animator animator;
	protected NavMeshAgent nav;
	protected SphereCollider col;

    protected float attackCooldownCounter = 1;

	
	void Awake ()
	{

	}

	void Update () {

	}


    public void attackEntity(GameObject other){
		Debug.Log ("attack target " + other);
		//monsterScript monster = GetComponent<monsterScript> ();
		//bullet = monster.GetBullet();
		//Debug.Log ("bullet " + bullet);
		//Instantiate(bullet, new Vector3(-20, 0, 0), Quaternion.identity);
		attackCooldownCounter = attackCooldown;
    }

    //get distance between this entity and another object
    public float getDistanceFrom (Transform other)
    {
        return Vector3.Distance(other.position, transform.position);
    }

    float CalculatePathLength (Vector3 targetPosition)
    {
        // Create a path and set it based on a target position.
        NavMeshPath path = new NavMeshPath();
        if(nav.enabled)

            nav.CalculatePath(targetPosition, path);
        
        // Create an array of points which is the length of the number of corners in the path + 2.
        Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];
        
        // The first point is the enemy's position.
        allWayPoints[0] = transform.position;
        
        // The last point is the target position.
        allWayPoints[allWayPoints.Length - 1] = targetPosition;
        
        // The points inbetween are the corners of the path.
        for(int i = 0; i < path.corners.Length; i++)
        {
            allWayPoints[i + 1] = path.corners[i];
        }
        
        // Create a float to store the path length that is by default 0.
        float pathLength = 0;
        
        // Increment the path length by an amount equal to the distance between each waypoint and the next.
        for(int i = 0; i < allWayPoints.Length - 1; i++)
        {
            pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
        }
        
        return pathLength;
    }

	public virtual void setTarget(GameObject other) {
		Debug.Log ("shit");
	}

    //turn the sprite in the right direction
	public void refreshAnimator (){
			float angle = Vector3.Angle (nav.velocity.normalized, this.transform.forward);
			if (nav.velocity.normalized.x < this.transform.forward.x) {
					angle *= -1;
			}
			angle = (angle) % 360.0f;

			// Si l'agent ne bouge pas il se fixe sur l'animation repos la plus proche (voir controller)
			if (nav.velocity.x == 0) {
				animator.SetInteger ("Direction", 4);
			// S'il va au Sud
			} else if (angle <= -90) {
				animator.SetInteger ("Direction", 0);
			// S'il va vers l'Ouest
			} else if (angle < 0) {
				animator.SetInteger ("Direction", 1);
			// S'il va au Nord
			} else if (angle < 90) {
				animator.SetInteger ("Direction", 2);
			// S'il va vers l'Est
			} else if (angle <= 180) {
				animator.SetInteger ("Direction", 3);
			}
	}
}