﻿using UnityEngine;
using System.Collections.Generic;

public class enemyBuilding : MonoBehaviour {

	public Transform monster;
	public Vector3 buildingPosition = new Vector3(0,0,0); //current building position
	public int iaCountDownRefresh = 4;

	private float iaCountDown = 0f;

	private float resource = 0f;
	private List<GameObject> mobList = new List<GameObject>(); //list of generated mobs

	private GameObject createMob (int mobType){
		// do mobType selection here.

		GameObject mob = (GameObject)Instantiate(monster, buildingPosition, Quaternion.identity);
		mobList.Add(mob);
		mob.GetComponent<monsterScript>().parentList = mobList;
		return mob;
	}


	void Awake () {
		
	}

	// Update is called once per frame
	void Update () {

		//Refresh ia every iaCountDown seconds
		if (iaCountDown < iaCountDownRefresh){

		} else {
			iaCountDown += Time.deltaTime;
		}

		//add resource
		resource += Time.deltaTime;
	}
}
