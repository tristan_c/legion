﻿using UnityEngine;
using System.Collections;

public class spawnRobots : MonoBehaviour {
	
	private float countdown;
	public Transform robot;
	public float countDownSize = 1;
	
	// Use this for initialization
	void Start () {
		countdown = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (countdown <= 0) {
			Instantiate(robot, new Vector3(-1,1,-1), Quaternion.identity);
			//Instantiate(monster, new Vector3(0F, -2F, -6), Quaternion.identity);
			countdown += countDownSize;
		} else {
			countdown -= Time.deltaTime;
		}
	}
}
