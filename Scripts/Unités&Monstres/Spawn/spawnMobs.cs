﻿using UnityEngine;
using System.Collections;

public class spawnMobs : MonoBehaviour {

	private float countdown;
	public Transform monster;
	public float spawnArea = 8;
	public float countDownSize = 1;
	public float positionX;
	public float positionZ;

	// Use this for initialization
	void Start () {
		countdown = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (countdown <= 0) {
			positionX=Random.Range(-spawnArea,spawnArea);
			positionZ=Random.Range(-spawnArea,spawnArea);
			Instantiate(monster, new Vector3(positionX, 1, positionZ), Quaternion.identity);
			//Instantiate(monster, new Vector3(0F, -2F, -6), Quaternion.identity);
			countdown += countDownSize;
		} else {
			countdown -= Time.deltaTime;
		}
	}
}
