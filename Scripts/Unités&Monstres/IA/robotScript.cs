﻿using UnityEngine;
using System.Collections;

public class robotScript : Entity {

	//------------robotLvl--------------

	private int robotLvl = 0;
	private int robotPercentLvl = 0; //percent of current robot lvl, if = 100 robot lvl up
	private int robotType; // 0 = soldier, 1 = medic, 2 = workers

	//------------IA----------------
	public GameObject iaTarget;


	
	private GameObject target; // monster to kill
	
	public float countDownSize = 1;
	private float countdown;
	private Vector3 whereToGo = new Vector3(25, 1, -25); //origin spawn position

	
	//-----------private functions--------

	public void lvlUp () {
			if (robotPercentLvl >= 100) {
				robotLvl += 1;
				robotPercentLvl = 0;
				Debug.Log("robot lvl up");
			}
	}

	void Awake(){
		nav =GetComponentInParent<NavMeshAgent>();
		nav.updateRotation = false;
		nav.SetDestination(whereToGo);
		Debug.Log ("whereToGo awake: " + whereToGo);
	}

	void Start(){
		countdown = 1;
	}

	void Update () {
		//check if target
		if ((target != null) && (countdown <= 0)) {
			Debug.Log ("target: " + target);
			countdown = countDownSize;
			if (getDistanceFrom (target.transform) <= attackRange) {
				//if in attackRange stop moving and attack
				nav.Stop ();
				if (attackCooldownCounter <= 0) {
					attackEntity (target);
				}
			} /*else
		// approch target
		nav.SetDestination (target.gameObject.GetComponent<Transform> ().position);*/

		} else {
			countdown -= Time.deltaTime;
			attackCooldownCounter -= Time.deltaTime;
			if (Input.GetMouseButtonDown (0)) {
				whereToGo = Input.mousePosition;
				nav.SetDestination(whereToGo);
				Debug.Log ("whereToGo: " + whereToGo);
			}
		}
	}


	public override void setTarget(GameObject other) {
		// stock it in target if no target acquired
		var script = other.GetComponent<Entity> ();
		if(target == null && script != null && script.isMonster){
			target = other.gameObject;
			Debug.Log("Robot: target acquired");
		}
	}

	/*public void moveUnit(){
		whereToGo = Input.mousePosition;
		isMouseClicked = true;
		Debug.Log ("isMouseClicked: " + isMouseClicked);
		Debug.Log ("whereToGo: " + whereToGo);
		
	}*/
}
