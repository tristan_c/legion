﻿using UnityEngine;
using System.Collections.Generic;

public class monsterScript : Entity {

	public List<GameObject> parentList;
	public Vector3 whereToGo = new Vector3(25, 0, -25);
	private GameObject target; // robot to kill

	void Awake(){
		isMonster = true;
		// Setting up the references.
		col = GetComponent<SphereCollider>();
		nav = GetComponent<NavMeshAgent>();
		nav.updateRotation = false;
		nav.SetDestination(whereToGo);
	}

	void Update () {
		nav.SetDestination(whereToGo);
		refreshAnimator();

		//check if target
		if (target != null) {
			if (getDistanceFrom (target.transform) <= attackRange) {
				//if in attackRange stop moving and attack
				nav.Stop();
				if (attackCooldownCounter <= 0) {
					attackEntity (target);
				}
			} else
				// approch target
				nav.SetDestination (target.gameObject.GetComponent<Transform> ().position);
		}
	
		attackCooldownCounter -= Time.deltaTime;
	}

	public override void setTarget(GameObject other) {
		// stock it in target if no target acquired
		var script = other.GetComponent<Entity> ();
		if(target == null && script != null && !script.isMonster){
			target = other.gameObject;
			Debug.Log("Monster: target acquired");
		}
		
        //Destroy(other.gameObject);
    }

	void OnDestroy()
	{
		parentList.Remove(this.gameObject);
	}

}
