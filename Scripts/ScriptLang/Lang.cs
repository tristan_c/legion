using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public class Lang : Singleton<Lang>
{
    private Dictionary<string, string> _gameTexts;

    public string this[string key]
    {
        get { return GetText(key); }
    }

    public string[] this[string[] keys]
    {
        get { return GetText(keys); }
    }
	

    public void ChangeDictionnary()
	{
		//string lang = Application.systemLanguage.ToString();
        string JsonString = string.Empty;

		//Récupération de la variable instancié dans le script LanguageManager.cs
		string Lang = PlayerPrefs.GetString ("langue");

		if (Lang == "French")
			JsonString = Resources.Load<TextAsset>("lang.fr").text;
		else if (Lang == "English")
			JsonString = Resources.Load<TextAsset>("lang.en").text;
		else if (Lang == "Deutsch")
			JsonString = Resources.Load<TextAsset>("lang.de").text;

		JSONNode json = JSON.Parse(JsonString);
        int size = json.Count;
       
        _gameTexts = new Dictionary<string, string>(size);

        JSONArray array;
        for (int i = 0; i < size; i++)
        {
            array = json[i].AsArray;
            _gameTexts.Add(array[0].Value, array[1].Value);
        }
    }

    public static string Get(string key)
    {
        return Instance.GetText(key);
    }

    public static string[] Get(string[] keys)
    {
        return Instance.GetText(keys);
    }

    public string GetText(string key)
    {
        if (_gameTexts.ContainsKey(key))
            return _gameTexts[key];

        return key;
    }
    
    public string[] GetText(string[] keys)
    {
    	int size = keys.Length;
    	string[] results = new string[size];
    	
    	for (int i = 0; i < size; i++)
    		results[i] = GetText(keys[i]);
    	
    	return results;
    }
}