using UnityEngine;
using System.Collections;

public class MapBuilder : MonoBehaviour {
	int Size_X, Size_Z;
	private int x, y;
	
	public GameObject NotWalkable3D;

	public GameObject RefTileMap;
	
	public int Type;

	void Start () {
		BuildMap ();
	}

	void BuildMap() {
		// récupération des informations depuis script Mapping
		Mapping carto = RefTileMap.GetComponent<Mapping> ();

		Type = carto.GetMapType ();
		Size_X = carto.GetMapSizeX ();
		Size_Z = carto.GetMapSizeZ ();

		/*
		// Création du ground3D
		ground3D = GameObject.CreatePrimitive(PrimitiveType.Quad);
		ground3D.transform.position = new Vector3(Size_X/2, -2, -Size_Z/2);
		ground3D.transform.rotation = Quaternion.Euler(90, 0, 0);
		ground3D.transform.localScale += new Vector3(Size_X, Size_Z, 1);*/

		//NotWalkable3D = GameObject.CreatePrimitive(PrimitiveType.Cube);

		// mise en place des cubes d'obstacles selon le Type de la carte
		for (y=0; y < Size_Z; y++) {
			for (x=0; x < Size_X; x++) {

				float Coord1 = x+0.5F;
				float Coord2 = -y-0.5F;

					// Marais
				if (Type==0) {

					if (carto.GetMap (x, y) == 3) {

						Instantiate(NotWalkable3D, new Vector3(Coord1, -1.5F, Coord2), Quaternion.identity);

					}

					// Désert
				} else if (Type==1) {

					if (carto.GetMap (x, y) == 3) {

						Instantiate(NotWalkable3D, new Vector3(Coord1, -1.5F, Coord2), Quaternion.identity);

					}
					// Montagne
				} else if (Type==2) {

					if (carto.GetMap (x, y) == 3) {

						Instantiate(NotWalkable3D, new Vector3(Coord1, -1.5F, Coord2), Quaternion.identity);

					}
					// Volcan
				} else if (Type==3) {

					if (carto.GetMap (x, y) == 4) {

						Instantiate(NotWalkable3D, new Vector3(Coord1, -1.5F, Coord2), Quaternion.identity);

					}
					// Brousse
				} else if (Type==4) {

					if (carto.GetMap (x, y) == 1) {

						Instantiate(NotWalkable3D, new Vector3(Coord1, -1.5F, Coord2), Quaternion.identity);

					}
					// City
				} else if (Type==5) {

					if (carto.GetMap (x, y) == 1) {

						Instantiate(NotWalkable3D, new Vector3(Coord1, -1.5F, Coord2), Quaternion.identity);

					}
					// Plaine
				} else {
					
				}
			}
		}
		Debug.Log ("Flag");
		BuildNewNavMesh ();

	}

	void BuildNewNavMesh () {

		GameObject[] obstacles = GameObject.FindGameObjectsWithTag("notWalkable3D");

		for(int i = 0; i < obstacles.Length; i++) {

			obstacles[i].GetComponent<NavMeshObstacle>().carving = true;
			Debug.Log ("Carving effectué");

		}

	}
}