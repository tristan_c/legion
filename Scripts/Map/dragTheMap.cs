﻿using UnityEngine;
using System.Collections;

public class DragTheMap : MonoBehaviour {

	private float dist;
	private Vector3 originMouse;

	void Start () {
		dist = transform.position.z; 
	}

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			originMouse = new Vector3(Input.mousePosition.x, dist, Input.mousePosition.z);
			originMouse = Camera.main.ScreenToWorldPoint (originMouse);
			originMouse.y = transform.position.z;
		}
		else if (Input.GetMouseButton (0)) {
			var v3Pos = new Vector3(Input.mousePosition.x, dist, Input.mousePosition.z);
			v3Pos = Camera.main.ScreenToWorldPoint (v3Pos);
			v3Pos.z = transform.position.z;
			transform.position -= (v3Pos - originMouse);
		}
	}
}
