﻿/* Ce script sert à dimensionner la taille de map grace
 * aux paramètres dans le script mapping lié à TileMap */

using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(Mapping))]
public class TileMapInspector : Editor {
	
	public override void OnInspectorGUI() {
		//base.OnInspectorGUI();
		DrawDefaultInspector();
		
		if(GUILayout.Button("Générer")) {
			Mapping TileMap = (Mapping)target;
			TileMap.BuildMesh();
		}
	}
}
