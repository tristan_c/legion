﻿using UnityEngine;
using System.Collections;

public class TileMap {

	// taille de la carte en x et y
	int Size_X, size_y;

	// tableau de coordonnées des tuiles de la carte
	int[,] Map_Data;

	// type de biome de la map
	int Type = Random.Range(0,6);

	public TileMap(int Size_X, int size_y) {
		this.Size_X = Size_X;
		this.size_y = size_y;
		Map_Data = new int[Size_X, size_y];

		makeTiles (0, 0, Size_X, size_y);
	}

	// getter des coordonnées des tuiles de la map
	public int GetTileAt(int x, int y){
		return Map_Data[x,y];
	}

	// getter du type de biome de la map
	public int GetMapType() {
		return Type;
	}

	public void makeTiles(int left, int top, int width, int height) {

		for (int x=0; x < width; x++) {
			for (int y=0; y < height; y++) {

				int coord1 = Random.Range(5,Size_X-5);
				int coord2 = Random.Range(5,size_y-5);

				/*
				 * 0 = desert
				 * 1 = grass
				 * 2 = rock
				 * 3 = water
				 * 4 = lava
				 * 5 = brick
				 */

				// Marais
				if (Type==0) {
					Map_Data [x, y] = 1;
					if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
						if (Map_Data[coord1-1,coord2] != 3 && Map_Data[coord1-1,coord2-1] != 3 && Map_Data[coord1,coord2-1] != 3 && Map_Data[coord1+1,coord2] != 3 && Map_Data[coord1+1,coord2+1] != 3 && Map_Data[coord1,coord2+1] != 3 && Map_Data[coord1-2,coord2] != 3 && Map_Data[coord1-2,coord2-2] != 3 && Map_Data[coord1,coord2-2] != 3 && Map_Data[coord1+2,coord2] != 3 && Map_Data[coord1+2,coord2+2] != 3 && Map_Data[coord1,coord2+2] != 3 && Map_Data[coord1-1,coord2+1] != 3 && Map_Data[coord1+1,coord2-1] != 3  && Map_Data[coord1-2,coord2+2] != 3 && Map_Data[coord1+2,coord2-2] != 3 && Map_Data[coord1-2,coord2-1] != 3 && Map_Data[coord1-1,coord2-2] != 3 && Map_Data[coord1+2,coord2+1] != 3 && Map_Data[coord1+1,coord2+2] != 3 && Map_Data[coord1-2,coord2+1] != 3 && Map_Data[coord1-1,coord2+2] != 3 && Map_Data[coord1+2,coord2-1] != 3 && Map_Data[coord1-2,coord2+1] != 3 && Map_Data[coord1+1,coord2-2] != 3) {

							int rand = Random.Range(0,5);
							if (rand == 1) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2] = 3;
								Map_Data [coord1+2, coord2+1] = 3;
							} else if (rand == 2) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1, coord2-1] = 3;
								Map_Data [coord1-1, coord2-1] = 3;
							} else if (rand == 3) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1, coord2+1] = 3;
								Map_Data [coord1, coord2+2] = 3;
								Map_Data [coord1+1, coord2+1] = 3;
							} else if (rand == 4) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2] = 3;
							} else if (rand == 5) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2+1] = 3;
							}
						}
					}

				// Désert
				} else if (Type==1) {
					Map_Data [x, y] = 0;
					if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
						if (Map_Data[coord1-1,coord2] != 2 && Map_Data[coord1-1,coord2-1] != 2 && Map_Data[coord1,coord2-1] != 2 && Map_Data[coord1+1,coord2] != 2 && Map_Data[coord1+1,coord2+1] != 2 && Map_Data[coord1,coord2+1] != 2 && Map_Data[coord1-2,coord2] != 2 && Map_Data[coord1-2,coord2-2] != 2 && Map_Data[coord1,coord2-2] != 2 && Map_Data[coord1+2,coord2] != 2 && Map_Data[coord1+2,coord2+2] != 2 && Map_Data[coord1,coord2+2] != 2 && Map_Data[coord1-1,coord2+1] != 2 && Map_Data[coord1+1,coord2-1] != 2  && Map_Data[coord1-2,coord2+2] != 2 && Map_Data[coord1+2,coord2-2] != 2 && Map_Data[coord1-2,coord2-1] != 2 && Map_Data[coord1-1,coord2-2] != 2 && Map_Data[coord1+2,coord2+1] != 2 && Map_Data[coord1+1,coord2+2] != 2 && Map_Data[coord1-2,coord2+1] != 2 && Map_Data[coord1-1,coord2+2] != 2 && Map_Data[coord1+2,coord2-1] != 2 && Map_Data[coord1-2,coord2+1] != 2 && Map_Data[coord1+1,coord2-2] != 2) {

							int rand = Random.Range(0,10);
							if (rand == 1) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2] = 3;
								Map_Data [coord1+2, coord2+1] = 3;
							} else if (rand == 2) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1, coord2-1] = 3;
								Map_Data [coord1-1, coord2-1] = 3;
							} else if (rand == 3) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1, coord2+1] = 3;
								Map_Data [coord1, coord2+2] = 3;
								Map_Data [coord1+1, coord2+1] = 3;
							} else if (rand == 4) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2] = 3;
							} else if (rand == 5) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2+1] = 3;
							}
						}
					}

				// Montagne
				} else if (Type==2) {
					Map_Data [x, y] = 2;
					if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
						if (Map_Data[coord1-1,coord2] != 3 && Map_Data[coord1-1,coord2-1] != 3 && Map_Data[coord1,coord2-1] != 3 && Map_Data[coord1+1,coord2] != 3 && Map_Data[coord1+1,coord2+1] != 3 && Map_Data[coord1,coord2+1] != 3 && Map_Data[coord1-2,coord2] != 3 && Map_Data[coord1-2,coord2-2] != 3 && Map_Data[coord1,coord2-2] != 3 && Map_Data[coord1+2,coord2] != 3 && Map_Data[coord1+2,coord2+2] != 3 && Map_Data[coord1,coord2+2] != 3 && Map_Data[coord1-1,coord2+1] != 3 && Map_Data[coord1+1,coord2-1] != 3  && Map_Data[coord1-2,coord2+2] != 3 && Map_Data[coord1+2,coord2-2] != 3 && Map_Data[coord1-2,coord2-1] != 3 && Map_Data[coord1-1,coord2-2] != 3 && Map_Data[coord1+2,coord2+1] != 3 && Map_Data[coord1+1,coord2+2] != 3 && Map_Data[coord1-2,coord2+1] != 3 && Map_Data[coord1-1,coord2+2] != 3 && Map_Data[coord1+2,coord2-1] != 3 && Map_Data[coord1-2,coord2+1] != 3 && Map_Data[coord1+1,coord2-2] != 3) {

							int rand = Random.Range(0,10);
							if (rand == 1) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2] = 3;
								Map_Data [coord1+2, coord2+1] = 3;
							} else if (rand == 2) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1, coord2-1] = 3;
								Map_Data [coord1-1, coord2-1] = 3;
							} else if (rand == 3) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1, coord2+1] = 3;
								Map_Data [coord1, coord2+2] = 3;
								Map_Data [coord1+1, coord2+1] = 3;
							} else if (rand == 4) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2] = 3;
							} else if (rand == 5) {
								Map_Data [coord1, coord2] = 3;
								Map_Data [coord1+1, coord2] = 3;
								Map_Data [coord1+2, coord2+1] = 3;
							}
						}
					}
				
				// Volcan
				} else if (Type==3) {
					Map_Data [x, y] = 2;
					if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
						if (Map_Data[coord1-1,coord2] != 4 && Map_Data[coord1-1,coord2-1] != 4 && Map_Data[coord1,coord2-1] != 4 && Map_Data[coord1+1,coord2] != 4 && Map_Data[coord1+1,coord2+1] != 4 && Map_Data[coord1,coord2+1] != 4 && Map_Data[coord1-2,coord2] != 4 && Map_Data[coord1-2,coord2-2] != 4 && Map_Data[coord1,coord2-2] != 4 && Map_Data[coord1+2,coord2] != 4 && Map_Data[coord1+2,coord2+2] != 4 && Map_Data[coord1,coord2+2] != 4 && Map_Data[coord1-1,coord2+1] != 4 && Map_Data[coord1+1,coord2-1] != 4  && Map_Data[coord1-2,coord2+2] != 4 && Map_Data[coord1+2,coord2-2] != 4 && Map_Data[coord1-2,coord2-1] != 4 && Map_Data[coord1-1,coord2-2] != 4 && Map_Data[coord1+2,coord2+1] != 4 && Map_Data[coord1+1,coord2+2] != 4 && Map_Data[coord1-2,coord2+1] != 4 && Map_Data[coord1-1,coord2+2] != 4 && Map_Data[coord1+2,coord2-1] != 4 && Map_Data[coord1-2,coord2+1] != 4 && Map_Data[coord1+1,coord2-2] != 4) {

							int rand = Random.Range(0,10);
							if (rand == 1) {
								Map_Data [coord1+1, coord2] = 4;
								Map_Data [coord1+2, coord2] = 4;
								Map_Data [coord1+2, coord2+1] = 4;
							} else if (rand == 2) {
								Map_Data [coord1, coord2-1] = 4;
								Map_Data [coord1-1, coord2-1] = 4;
							} else if (rand == 3) {
								Map_Data [coord1, coord2+1] = 4;
								Map_Data [coord1, coord2+2] = 4;
								Map_Data [coord1+1, coord2+1] = 4;
							} else if (rand == 4) {
								Map_Data [coord1+1, coord2] = 4;
								Map_Data [coord1+2, coord2] = 4;
							} else if (rand == 5) {
								Map_Data [coord1+1, coord2] = 4;
								Map_Data [coord1+2, coord2+1] = 4;
							}
						}
					}
				
				// Brousse
				} else if (Type==4) {
					Map_Data [x, y] = 0;
					if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
						if (Map_Data[coord1-1,coord2] != 1 && Map_Data[coord1-1,coord2-1] != 1 && Map_Data[coord1,coord2-1] != 1 && Map_Data[coord1+1,coord2] != 1 && Map_Data[coord1+1,coord2+1] != 1 && Map_Data[coord1,coord2+1] != 1 && Map_Data[coord1-2,coord2] != 1 && Map_Data[coord1-2,coord2-2] != 1 && Map_Data[coord1,coord2-2] != 1 && Map_Data[coord1+2,coord2] != 1 && Map_Data[coord1+2,coord2+2] != 1 && Map_Data[coord1,coord2+2] != 1 && Map_Data[coord1-1,coord2+1] != 1 && Map_Data[coord1+1,coord2-1] != 1  && Map_Data[coord1-2,coord2+2] != 1 && Map_Data[coord1+2,coord2-2] != 1 && Map_Data[coord1-2,coord2-1] != 1 && Map_Data[coord1-1,coord2-2] != 1 && Map_Data[coord1+2,coord2+1] != 1 && Map_Data[coord1+1,coord2+2] != 1 && Map_Data[coord1-2,coord2+1] != 1 && Map_Data[coord1-1,coord2+2] != 1 && Map_Data[coord1+2,coord2-1] != 1 && Map_Data[coord1-2,coord2+1] != 1 && Map_Data[coord1+1,coord2-2] != 1) {

							int rand = Random.Range(0,10);
							if (rand == 1) {
								Map_Data [coord1+1, coord2] = 1;
								Map_Data [coord1+2, coord2] = 1;
								Map_Data [coord1+2, coord2+1] = 1;
							} else if (rand == 2) {
								Map_Data [coord1, coord2-1] = 1;
								Map_Data [coord1-1, coord2-1] = 1;
							} else if (rand == 1) {
								Map_Data [coord1, coord2+1] = 1;
								Map_Data [coord1, coord2+2] = 1;
								Map_Data [coord1+1, coord2+1] = 1;
							} else if (rand == 4) {
								Map_Data [coord1+1, coord2] = 1;
								Map_Data [coord1+2, coord2] = 1;
							} else if (rand == 5) {
								Map_Data [coord1+1, coord2] = 1;
								Map_Data [coord1+2, coord2+1] = 1;
							}
						}
					}
				
				// City
				} else if (Type==5) {
					Map_Data [x, y] = 5;
					if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
						if (Map_Data[coord1-1,coord2] != 1 && Map_Data[coord1-1,coord2-1] != 1 && Map_Data[coord1,coord2-1] != 1 && Map_Data[coord1+1,coord2] != 1 && Map_Data[coord1+1,coord2+1] != 1 && Map_Data[coord1,coord2+1] != 1 && Map_Data[coord1-2,coord2] != 1 && Map_Data[coord1-2,coord2-2] != 1 && Map_Data[coord1,coord2-2] != 1 && Map_Data[coord1+2,coord2] != 1 && Map_Data[coord1+2,coord2+2] != 1 && Map_Data[coord1,coord2+2] != 1 && Map_Data[coord1-1,coord2+1] != 1 && Map_Data[coord1+1,coord2-1] != 1  && Map_Data[coord1-2,coord2+2] != 1 && Map_Data[coord1+2,coord2-2] != 1 && Map_Data[coord1-2,coord2-1] != 1 && Map_Data[coord1-1,coord2-2] != 1 && Map_Data[coord1+2,coord2+1] != 1 && Map_Data[coord1+1,coord2+2] != 1 && Map_Data[coord1-2,coord2+1] != 1 && Map_Data[coord1-1,coord2+2] != 1 && Map_Data[coord1+2,coord2-1] != 1 && Map_Data[coord1-2,coord2+1] != 1 && Map_Data[coord1+1,coord2-2] != 1) {

							int rand = Random.Range(0,10);
							if (rand == 1) {
								Map_Data [coord1+1, coord2] = 1;
								Map_Data [coord1+2, coord2] = 1;
								Map_Data [coord1+2, coord2+1] = 1;
							} else if (rand == 2) {
								Map_Data [coord1, coord2-1] = 1;
								Map_Data [coord1-1, coord2-1] = 1;
							} else if (rand == 1) {
								Map_Data [coord1, coord2+1] = 1;
								Map_Data [coord1, coord2+2] = 1;
								Map_Data [coord1+1, coord2+1] = 1;
							} else if (rand == 4) {
								Map_Data [coord1+1, coord2] = 1;
								Map_Data [coord1+2, coord2] = 1;
							} else if (rand == 5) {
								Map_Data [coord1+1, coord2] = 1;
								Map_Data [coord1+2, coord2+1] = 1;
							}
						}
					}

				// Plaine
				} else {
					Map_Data [x, y] = 1;
				}
			}
		}
	}
}