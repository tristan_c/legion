﻿using UnityEngine;
using System.Collections;

// Ajout des composants obligatoires
[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class Mapping : MonoBehaviour {
	
	public int Size_X;
	public int Size_Z;
	public float Tilesize = 1.0f;

	public Texture2D TerrainTiles;
	public int TileResolution=32;

	// Déclaration des éléments de tileMap
	public TileMap Map;
	public int Type;
	// tableau de coordonnées des tuiles de la carte
	int[,] Map_Data;

	// initialisation
	void Start () {
		if (Map == null)
			BuildMesh();
	}

	// découpage des tuiles
	Color[][] ChopUpTiles() {
		int NumTilesPerRow = TerrainTiles.width / TileResolution;
		int NumRows = TerrainTiles.height / TileResolution;
		
		Color[][] Tiles = new Color[NumTilesPerRow * NumRows][];
		
		for (int y=0; y < NumRows; y++) {
			for (int x=0; x < NumTilesPerRow; x++) {
				Tiles[y*NumTilesPerRow + x] = TerrainTiles.GetPixels(x*TileResolution, y*TileResolution, TileResolution, TileResolution);
			}
		}
		return Tiles;
	}
	
	void BuildTexture() {
		Map = new TileMap (Size_X, Size_Z);
		int TexWidth = Size_X * TileResolution;
		int TexHeight = Size_Z * TileResolution;
		Texture2D texture = new Texture2D(TexWidth, TexHeight);
		
		Color[][] Tiles = ChopUpTiles ();

		for (int y=0; y < Size_Z; y++) {
			for (int x=0; x < Size_X; x++) {
				// Récupération des tuiles de la Map de GetTileAt dans tileMap
				Color[] p = Tiles[Map.GetTileAt(x,y)];
				texture.SetPixels(x*TileResolution, y*TileResolution, TileResolution, TileResolution, p);
			}
		}

		// quelques filtres pour améliorer le rendu
		texture.filterMode = FilterMode.Bilinear;
		texture.wrapMode = TextureWrapMode.Clamp;
		texture.Apply ();
		
		MeshRenderer Mesh_Renderer = GetComponent<MeshRenderer> ();
		Mesh_Renderer.sharedMaterials[0].mainTexture = texture;
	}

	public void BuildMesh() {
		// nombres de tuiles
		int NumTiles = Size_X * Size_Z;
		// nombre de triangles (1 tuile = 2 triangles)
		int NumTris = NumTiles * 2;
		
		int vSize_X = Size_X + 1;
		int vSize_Z = Size_Z + 1;
		int NumVerts = vSize_X * vSize_Z;
		
		// Génération des données du mesh
		Vector3[] vertices = new Vector3[ NumVerts ];
		Vector3[] normals = new Vector3[NumVerts];
		/* uv.x = (float)x / vSize_X
		x=0, uv.x = 0
		x=101, uv.x = 1 */
		Vector2[] uv = new Vector2[NumVerts];
		
		int[] triangles = new int[ NumTris * 3 ];
		
		for(int z=0; z < vSize_Z; z++) {
			for(int x=0; x < vSize_X; x++) {
				vertices[ z * vSize_X + x ] = new Vector3( x*Tilesize, 0, -z*Tilesize );
				normals[ z * vSize_X + x ] = Vector3.up;
				uv[ z * vSize_X + x ] = new Vector2( (float)x / Size_X, (float)z / Size_Z );
			}
		}
		
		for(int z=0; z < Size_Z; z++) {
			for(int x=0; x < Size_X; x++) {
				int squareIndex = z * Size_X + x;
				int triOffset = squareIndex * 6;
				triangles[triOffset + 0] = z * vSize_X + x + 		   0;
				triangles[triOffset + 2] = z * vSize_X + x + vSize_X + 0;
				triangles[triOffset + 1] = z * vSize_X + x + vSize_X + 1;
				
				triangles[triOffset + 3] = z * vSize_X + x + 		   0;
				triangles[triOffset + 5] = z * vSize_X + x + vSize_X + 1;
				triangles[triOffset + 4] = z * vSize_X + x + 		   1;
			}
		}

		// Création d'un nouveau mesh avec les données
		Mesh mesh = new Mesh ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uv;
		
		// Agnation du mesh aux composants
		MeshFilter mesh_filter = GetComponent<MeshFilter>();
		MeshRenderer Mesh_Renderer = GetComponent<MeshRenderer>();
		MeshCollider mesh_collider = GetComponent<MeshCollider>();
		
		mesh_filter.mesh = mesh;
		mesh_collider.sharedMesh = mesh;	
		
		BuildTexture ();
	}

	// getters taille de la carte
	public int GetMapSizeX()
	{
		return Size_X;
	}
	
	public int GetMapSizeZ()
	{
		return Size_Z;
	}
	
	// getter du type de biome de la Map
	public int GetMapType()
	{
		if(Map == null)
			BuildMesh();
		Type = Map.GetMapType ();
		return Type;
	}
	
	// getter des coordonnées des tuiles de la Map
	public int GetMap(int x, int y){
		return Map.GetTileAt(x,y);
	}
}

