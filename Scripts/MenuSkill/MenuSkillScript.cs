﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuSkillScript : MonoBehaviour {
	
	Image[] ImageValue;
	public GameObject SkillMenuCanvas;
	bool IsPreviousSkillActivate = false;


	void Awake(){
		ImageValue = SkillMenuCanvas.GetComponentsInChildren<Image>();
	}
	
	public void OnSkillClick1() {
		ImageValue [1].color = Color.green;
		IsPreviousSkillActivate = true;

	}

	public void OnSkillClick2() {
		if(IsPreviousSkillActivate) {
			ImageValue [2].color = Color.green;
		}
	}

	public void OnSkillClick3() {
		if(IsPreviousSkillActivate) {
			ImageValue [3].color = Color.green;
		}
	}
}