﻿using UnityEngine;
using System.Collections;

public class UIManagerScript : MonoBehaviour {

	//Boutons du menu principal
	public Animator NewGameButton;
	public Animator ContinueButton;
	public Animator SettingsButton;
	public Animator QuitButton;
	
	//Boutons du panneau des options
	public Animator SettingsText;
	public Animator LanguageButton;
	public Animator VideoButton;
	public Animator SoundsButton;
	public Animator CommandsButton;
	public Animator QuitSettingsButton;
	public Animator SettingsPanel;
	
	//Boutons du panel d'option choisi
	public GameObject SoundSettings;
	public GameObject LanguageSettings;


	//-----------------------------------------------------------------------------

	//On active les panels d'options pour permettre au script LanguageManager
	//de récupérer les composants du Canvas
	void Awake()
	{
		SoundSettings.SetActive(true);
		LanguageSettings.SetActive(true);
	}

	//Puis on le désactive pour éviter l'overlap
	public void Start()
	{
		SoundSettings.SetActive(false);
		LanguageSettings.SetActive(false);
	}

	void Update () 
	{
		//Fermer le menu des options si Echap est pressé
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			CloseSettings();
		}
	}

	public void StartGame()
	{
		Application.LoadLevel("navmesh");
	}

	public void QuitGame()
	{
		Application.Quit ();
	}

	//Lors du clic sur le boutons options
	public void OpenSettings()
	{
		NewGameButton.SetBool("isShownMenu", true);
		ContinueButton.SetBool("isShownMenu", true);
		SettingsButton.SetBool("isShownMenu", true);
		QuitButton.SetBool("isShownMenu", true);

		SettingsText.SetBool("isShownMenu", false);
		LanguageButton.SetBool("isShownMenu", false);
		VideoButton.SetBool("isShownMenu", false);
		SoundsButton.SetBool("isShownMenu", false);
		CommandsButton.SetBool("isShownMenu", false);
		QuitSettingsButton.SetBool("isShownMenu", false);
		SettingsPanel.SetBool("isShownMenu", false);
	}

	//Lors du clic sur le boutons de fermeture des options
	public void CloseSettings()
	{
		NewGameButton.SetBool("isShownMenu", false);
		ContinueButton.SetBool("isShownMenu", false);
		SettingsButton.SetBool("isShownMenu", false);
		QuitButton.SetBool("isShownMenu", false);

		SettingsText.SetBool("isShownMenu", true);
		LanguageButton.SetBool("isShownMenu", true);
		VideoButton.SetBool("isShownMenu", true);
		SoundsButton.SetBool("isShownMenu", true);
		CommandsButton.SetBool("isShownMenu", true);
		QuitSettingsButton.SetBool("isShownMenu", true);
		SettingsPanel.SetBool("isShownMenu", true);

		SoundSettings.SetActive(false);
		LanguageSettings.SetActive(false);
	}

	public void DisplaySoundSettings()
	{
		//Suppression de l'affichage des autres options
		LanguageSettings.SetActive(false);

		//Affichage de l'option souhaité
		SoundSettings.SetActive(true);
	}

	public void DisplayLanguageSettings()
	{
		//Suppression de l'affichage des autres options
		SoundSettings.SetActive(false);
		
		//Affichage de l'option souhaité
		LanguageSettings.SetActive(true);
	}
}