﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LanguageManager : MonoBehaviour {

	//Récupération du GameObject contenant Lang.cs
	public Transform ScriptLang;
	private Text[] TextValue;
	public GameObject Canvas;


	void Awake() 
	{
		//Récupération des éléments du menu
		TextValue = Canvas.GetComponentsInChildren<Text>();
	}


	public void ChangeLanguageToFrench()
	{
		//Changement de la langue d'affichage voulue
		//Transmission au script Lang.cs
		PlayerPrefs.SetString("langue", "French");
	}

	
	public void ChangeLanguageToEnglish()
	{
		//Changement de la langue d'affichage voulue
		//Transmission au script Lang.cs
		PlayerPrefs.SetString("langue", "English");
	}

	public void ChangeLanguageToDeutsch()
	{
		//Changement de la langue d'affichage voulue
		//Transmission au script Lang.cs
		PlayerPrefs.SetString("langue", "Deutsch");
	}


	public void ChangeDisplayLanguage() 
	{
		//Récupération du script Lang.cs et affection à une variable lang
		//Appel de la fonction ChangeDictionnary() présente dans Lang.cs
		Lang lang = ScriptLang.GetComponent<Lang> ();
		lang.ChangeDictionnary();

		
		//Liste des objets contenus dans le canvas
		//Boutons Menu Principal
		TextValue[1].text = Lang.Get("menu.new");
		TextValue[2].text = Lang.Get("menu.continue");
		TextValue[3].text = Lang.Get("menu.settings");
		TextValue[4].text = Lang.Get("menu.exit");
		
		//Boutons du volet des options
		TextValue[5].text = Lang.Get("menu.settings_title");
		TextValue[6].text = Lang.Get("menu.language");
		TextValue[7].text = Lang.Get("menu.video");
		TextValue[8].text = Lang.Get("menu.sounds");
		TextValue[9].text = Lang.Get("menu.commands");
		TextValue[10].text = Lang.Get("menu.exit_settings");
		
		//Panel des options - Languages
		TextValue[11].text = Lang.Get("menu.language_title");
		//Indice 12 à 14 inchangé (écrit dans la langue d'origine)


		//Panel des options - Sounds
		TextValue[15].text = Lang.Get("menu.sounds_toggle");
		TextValue[16].text = Lang.Get("menu.sounds_volume");
		TextValue[17].text = Lang.Get("menu.music_toggle");
		TextValue[18].text = Lang.Get("menu.music_volume");

	}
}
