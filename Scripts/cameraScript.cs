﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour {

	public Vector3 speed = new Vector3(7, 7);

	//Zoom in/out
	public float zoomSpeed = 15;
	public float targetOrtho;
	public float smoothSpeed = 40.0f;
	public float minOrtho = 1.0f;
	public float maxOrtho = 20.0f;
	// Défilement par drag souris
	private float dist;
	private Vector3 originMouse;
	private float movementX;
	private float movementY;
	/*// Défilement aux bords de l'écran avec la souris
	private float mouseX;
	private float mouseY;
	private int centerScreenX;
	private int centerScreenZ;
	private float centerX;
	private float centerZ;
	private float oneX;
	private float oneY;*/

	void Start () {
		dist = transform.position.y; 
		targetOrtho = Camera.main.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {

		//Zoom in/out
		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		if (scroll != 0.0f) {
			targetOrtho -= scroll * zoomSpeed;
			targetOrtho = Mathf.Clamp (targetOrtho, minOrtho, maxOrtho);
		}
		Camera.main.orthographicSize = Mathf.MoveTowards (Camera.main.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
		
		// Défilement par drag souris
		if (Input.GetMouseButtonDown (0)) {
		originMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, dist);
			originMouse = Camera.main.ScreenToWorldPoint (originMouse);
			//originMouse.z = transform.position.z;
			//originMouse.x = transform.position.x;
		}
		else if (Input.GetMouseButton (0)) {
			var v3Pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, dist);
			v3Pos = Camera.main.ScreenToWorldPoint (v3Pos);
			//v3Pos.y = transform.position.y;
			transform.position -= (v3Pos - originMouse);
		 }

		// Déplacement aux flèches
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");

		var movement = new Vector3(
			speed.x * inputX,
			speed.y * inputY);

		movement *= Time.deltaTime;
		Camera.main.transform.Translate(movement);


		// Défilement par souris sur bord
		/*
		mouseX = Input.mousePosition.x;
		mouseY = Input.mousePosition.y;
		
		
		centerScreenX = Screen.width / 2;
		centerScreenZ = Screen.height / 2;
		
		centerX = 1.0f/centerScreenX;
		centerZ = 1.0f/centerScreenZ;
		
		if (mouseX < 0) {
				mouseX = 0;
		}
		else if( mouseX > Screen.width )
			mouseX = Screen.width;
		
		if (mouseY < 0) {
					mouseY = 0;
			} else if (mouseY > Screen.height) {
					mouseY = Screen.height;
			}
		
		oneX = ((mouseX-centerScreenX)*centerX);
		oneY = ((mouseY-centerScreenZ)*centerZ);
		
		if (oneX > -0.80f || oneX < 0.80f) {
				oneX = 0.0f;
		} else {
			if( oneX < 0 )
			{
				oneX = 5 * (oneX + 0.80f);
			} else {
				oneX = 5 * (oneX - 0.80f);
			}
		}
		
		if (oneY > -0.80f || oneY < 0.80f) {
				oneY = 0.0f;
		} else {
			if( oneY < 0 )
			{
				oneY = 5 * (oneY + 0.80f);
			} else {
				oneY = 5 * (oneY - 0.80f);
			}
		}
		
		//Time.deltaTime
		//Debug.Log( oneX + ", " + oneY + " : " + Screen.width + ", " + Screen.height);
		movementX = Camera.main.transform.position.x;
		movementY = Camera.main.transform.position.y; 
		movementX += (oneX * 5) * Time.deltaTime;
		movementY += (oneY * 5) * Time.deltaTime;*/

	}
}