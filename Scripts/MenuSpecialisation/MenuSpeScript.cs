﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuSpeScript : MonoBehaviour {
	
	public GameObject PanelGauche;
	public GameObject PanelHealGO;
	public GameObject PanelWarGO;
	public GameObject PanelFarmGO;

	public GameObject TextAreaHeal;
	public GameObject TextAreaWar;
	public GameObject TextAreaFarm;
	
	private Toggle[] UnitToggle;
	private Text[] TextPanelHeal;
	private Text[] TextPanelWar;
	private Text[] TextPanelFarm;

	private int NbUnitHeal = 1;
	private int NbUnitWar = 1;
	private int NbUnitFarm = 1;
	
		
	void Start(){
		UnitToggle = PanelGauche.GetComponentsInChildren<Toggle>();
		TextPanelHeal = PanelHealGO.GetComponentsInChildren<Text> ();
		TextPanelWar = PanelWarGO.GetComponentsInChildren<Text> ();
		TextPanelFarm = PanelFarmGO.GetComponentsInChildren<Text> ();
		TextAreaHeal.SetActive (false);
		TextAreaWar.SetActive (false);
		TextAreaFarm.SetActive (false);
	}


	public void WriteTextHeal(string NbUnit) {
		TextAreaHeal.SetActive (true);
		TextPanelHeal[1].text = NbUnit;
		NbUnitHeal ++;
	}

	public void WriteTextWar(string NbUnit) {
		TextAreaWar.SetActive (true);
		TextPanelWar[1].text = NbUnit;
		NbUnitWar ++;
	}

	public void WriteTextFarm(string NbUnit) {
		TextAreaFarm.SetActive (true);
		TextPanelFarm[1].text = NbUnit;
		NbUnitFarm ++;
	}

	public void OnPanelHealClicked() {
		foreach (Toggle Toggle in UnitToggle) {
			if (Toggle.isOn){
				GameObject Container = Toggle.transform.parent.gameObject;
				Container.SetActive(false);
				Toggle.isOn = false;
				WriteTextHeal(NbUnitHeal.ToString());
			}
		}
	}

	public void OnPanelWarClicked() {
		foreach (Toggle Toggle in UnitToggle) {
			if (Toggle.isOn){
				GameObject Container = Toggle.transform.parent.gameObject;
				Container.SetActive(false);
				Toggle.isOn = false;
				WriteTextWar(NbUnitWar.ToString());
			}
		}
	}

	public void OnPanelFarmClicked() {
		foreach (Toggle Toggle in UnitToggle) {
			if (Toggle.isOn){
				GameObject Container = Toggle.transform.parent.gameObject;
				Container.SetActive(false);
				Toggle.isOn = false;
				WriteTextFarm(NbUnitFarm.ToString());
			}
		}
	}
}