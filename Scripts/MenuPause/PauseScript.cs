﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {
	
	public Canvas PauseCanvas;
	public bool Paused;
	
	void Start() {
		PauseCanvas.enabled = false;
		Paused = false;
	}
	
	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) //check if Escape key/Back key is pressed
		{
			if (Paused){
				Paused = false;  //unpause the game if already paused
				
			} else {
				Paused = true;  //pause the game if not paused
				
			}
		}
		
		if (Paused) {
			Time.timeScale = 0;  //set the timeScale to 0 so that all the procedings are halted
			PauseCanvas.enabled = true;
		} else {
			Time.timeScale = 1;  //set it back to 1 on unpausing the game
			PauseCanvas.enabled = false;
		}
	}
	
	
	public void PauseGame() {
		//Duplication pour conserver le meme fonctionnement pour l'appui sur le bouton menu
		
		if (Paused){
			Paused = false;  //unpause the game if already paused
			Time.timeScale = 0;  //set the timeScale to 0 so that all the procedings are halted
			PauseCanvas.enabled = true;
		} else {
			Paused = true;  //pause the game if not paused
			Time.timeScale = 1;  //set it back to 1 on unpausing the game
			PauseCanvas.enabled = false;
		}
	}
	
	public void ResumeGame() {
		Paused = false;
	}
	
	public void QuitToMainMenu() {
		Application.LoadLevel ("Menu");
	}
}