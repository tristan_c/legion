﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class HUDScript : MonoBehaviour {

	//Récupération du GameObject contenant Lang.cs
	public Transform ScriptPause;
	public Canvas SpeCanvas;
	public Canvas CompCanvas;


	void Start() {
		SpeCanvas.enabled = false;
		CompCanvas.enabled = false;
	}

	//-----------------------------------------------------------
	//Fonctions de mise en pause du jeu

	public void PauseGame(){
		//Mise en pause du jeu en arrière plan
		PauseScript Pause = ScriptPause.GetComponent<PauseScript> ();
		Pause.PauseGame();
	}

	public void ResumeGame(){
		PauseScript Pause = ScriptPause.GetComponent<PauseScript> ();
		Pause.ResumeGame();
	}

	//-----------------------------------
	//Ouverture/fermeture du menu spécialisation

	public void OpenSpecialisation() {
		SpeCanvas.enabled = true;
		PauseGame ();
	}
	
	public void CloseSpecilisation() {
		SpeCanvas.enabled = false;
		ResumeGame ();
	}

	//-----------------------------------
	//Ouverture/fermeture du menu compétences

	public void OpenCompetences(){
		CompCanvas.enabled = true;
		PauseGame ();
	}

	public void CloseCompetences(){
		CompCanvas.enabled = false;
		ResumeGame ();
	}

	//-----------------------------------
}